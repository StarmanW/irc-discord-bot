'use strict';

const IRC = require('./includes/irc');

try {
    const config = require('./config.json'),
        irc = new IRC(config.irc_client.server, config.irc_client.nick, config.irc_client.options, config);

    irc.init();
} catch (err) {
    console.log(err);
}