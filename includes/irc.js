'use strict';

const {Client} = require('irc'),
    fs = require('fs'),
    Discord = require('./discord'),
    Logger = require('../modules/logger'),
    EVENTS = [
        'registered',
        'motd',
        'join',
        'part',
        'kick',
        'quit',
        'message',
        'error'
    ];

/**
 * Main IRC class that extends the
 * library's Client class.
 *
 * @class IRC
 * @extends {Client}
 */
class IRC extends Client {

    /**
     * Constructor method
     * @constructor
     * @param {*} server The IRC Server
     * @param {*} nick The nickname of the IRC account
     * @param {*} options IRC options, refer to https://node-irc.readthedocs.io/en/latest/API.html#irc.Client for more information
     * @param {*} config Configuration object from the config.json file
     * @memberof IRC
     */
    constructor(server, nick, options, config) {
        super(server, nick, options);
        this.config = config;
        this.channels = new Map();
        this.logger = Logger;
        this.discord = new Discord(config);
    }

    /**
     * Performs initialization of channels mapping
     * @memberof IRC
     * @returns {void}
     */
    init() {
        fs.readdir('./channels', (err, files) => {
            if (err) console.log(err);

            // Filter out non-JS files
            let jsFiles = files.filter((f) => f.endsWith('.js'));

            // Log message if no JS files
            if (jsFiles.length === 0) return console.log('No channels loaded.');

            jsFiles.forEach((f) => {
                let chan = new (require(`../channels/${f}`))(this);
                this.channels.set(chan.channel_name, chan);
                console.log(`Channel module ${f} successfully loaded!`);
            });

            console.log(`Successfully loaded ${jsFiles.length} channel modules.`);
        });

        // Bind each events to each of the corresponding method
        EVENTS.forEach((e) => {
            this.on(e, this[`_${e}`].bind(this));
        });
    }

    /**
     * REGISTERED event handler method
     * @param {Object} msg The message object received, refer to https://node-irc.readthedocs.io/en/latest/API.html#'raw'
     * @memberof IRC
     * @returns {void}
     */
    _registered(msg) {
        console.log(`Bot Registered: ${msg.args[1]}`);
    }

    /**
     * MOTD event handler method
     * @param {string} motd The message of the day sent from the server
     * @memberof IRC
     * @returns {void}
     */
    _motd(motd) {
        this.send('NICK', this.config.irc_nick);
        this.config.channels.forEach((c) => {
            this.join(c);
        });
    }

    /**
     * JOIN event handler method
     * @param {string} channel The name of the channel the bot joined
     * @param {string} nick The nickname of the bot
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof IRC
     * @returns {void}
     */
    _join(channel, nick, message) {
        console.log(`${nick} has joined ${channel}`);
    }

    /**
     * PART event handler method
     * @param {string} channel The name of the channel the bot part
     * @param {string} nick The nickname of the bot
     * @param {string} reason The reason the bot part
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof IRC
     * @returns {void}
     */
    _part(channel, nick, reason, message) {
        console.log(`${nick} has parted ${channel}. Reason: ${reason ? 'No reason' : reason}`);
        if (nick === this.config.irc_nick) {
            this.send('NICK', this.config.irc_nick);
        }
    }

    /**
     * KICK event handler method
     * @param {string} channel The name of the channel the bot is kicked from
     * @param {string} nick The nickname of the bot
     * @param {string} by The nickname of the person who kick the bot
     * @param {string} reason Reason of the kick
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof IRC
     * @returns {void}
     */
    _kick(channel, nick, by, reason, message) {
        console.log(`${nick} has been kicked by ${by} from ${channel}. Reason: ${reason ? 'No reason' : reason}`);
    }

    /**
     * QUIT event handler method
     * @param {string} nick The nickname of the bot
     * @param {string} reason Reason of the quit
     * @param {Array<string>} channels The list of channels the bot were in
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof IRC
     * @returns {void}
     */
    _quit(nick, reason, channels, message) {
        console.log(`${nick} has quit from IRC. Reason: ${reason}`);
        if (nick === this.config.irc_nick) {
            this.send('NICK', this.config.irc_nick);
        }
    }

    /**
     * MESSAGE event handler method
     * @param {string} nick The nickname of the sender
     * @param {string} to The channel/nickname of the message is intended to
     * @param {string} text The content of the message
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @returns
     * @memberof IRC
     * @returns {void}
     */
    _message(nick, to, text, message) {
        // Ignore any DM
        if (!to.startsWith('#')) return;

        let chann = this.channels.get(to.slice(1));
        if (chann) chann.run(nick, to, text, message);
    }

    /**
     * ERROR event handler method
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof IRC
     * @returns {void}
     */
    _error(message) {
        console.error(message);
    }
}

module.exports = IRC;