'use strict';

const rp = require('request-promise-native'),
    USER_AGENT = `${process.env.npm_package_name} - v${process.env.npm_package_version}`;

/**
 * Discord class
 * @class Discord
 */
class Discord {

    /**
     * Constructor method
     * @constructor
     * @param {*} config Configuration object
     * @memberof Discord
     */
    constructor(config) {
        this.config = config;
        this._initWebhook();
    }

    /**
     *
     *
     * @memberof Discord
     * @returns {void}
     */
    _initWebhook() {
        this._webhookURL = `https://discordapp.com/api/webhooks/${this.config.discord.id}/${this.config.discord.token}`;
        this._username = this.config.discord.username || 'Discord Bot';
        this._avatar_url = this.config.discord.avatar_url || '';
    }

    /**
     * Make HTTP request to discord webhook
     * @param {string} msg The content of the message to be sent to discord
     * @returns
     * @memberof Discord
     * @returns {Promise} Returns a new promise
     */
    async send(msg) {
        // Options for making HTTP request to Discord webhook
        const OPTIONS = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'User-Agent': USER_AGENT
            },
            uri: this._webhookURL,
            body: {
                content: msg,
                username: this._username,
                avatar_url: this._avatar_url
            },
            json: true // Automatically stringifies body to JSON
        };

        return rp(OPTIONS);
    }
}

module.exports = Discord;