'use strict';

/**
 * Newwikis class
 * @class NewWikis
 */
class NewWikis {

    /**
     * Constructor method
     * @constructor
     * @param {IRC} client Receive the client object
     * @memberof NewWikis
     */
    constructor(client) {
        this.channel_name = 'cvn-wikia-newwikis';
        this.client = client;
    }

    /**
     * Run Method
     * @param {string} nick The nickname of the sender
     * @param {string} to The channel/nickname of the message is intended to
     * @param {string} text The content of the message
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof NewWikis
     * @returns {void}
     */
    async run(nick, to, text, message) {
        // Strip out invisible control characters
        let msg = text.replace(/New\sWiki!\s(.*)/, 'New Wiki! <$1>');

        try {
            await this.client.discord.send(msg);
        } catch (err) {
            if (err.statusCode === 429) {
                setTimeout(async () => {
                    await this.client.discord.send(err.options.body.content);
                }, err.error.retry_after);
            }
        }
    }
}

module.exports = NewWikis;