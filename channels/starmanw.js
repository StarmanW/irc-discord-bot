'use strict';

/**
 * StarmanW class
 * @class StarmanW
 */
class StarmanW {

    /**
     * Constructor method
     * @constructor
     * @param {IRC} client Receive the client object
     * @memberof StarmanW
     */
    constructor(client) {
        this.channel_name = 'starmanw';
        this.client = client;
    }

    /**
     * Run method
     * @param {string} nick The nickname of the sender
     * @param {string} to The channel/nickname of the message is intended to
     * @param {string} text The content of the message
     * @param {Object} message The message object, refer to 'raw' event for more information
     * @memberof StarmanW
     * @returns {void}
     */
    async run(nick, to, text, message) {
        try {
            await this.client.discord.send(text);
        } catch (err) {
            console.log(err);
            if (err.statusCode === 429) {
                setTimeout(async () => {
                    await this.client.discord.send(err.options.body.content);
                }, err.error.retry_after);
            }
        }
    }
}

module.exports = StarmanW;